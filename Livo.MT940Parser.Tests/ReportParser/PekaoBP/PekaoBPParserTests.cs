﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportParser.PekaoBP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Livo.MT940Parser.Tests.ReportParser.PekaoBP
{
    public class PekaoBPParserTests
    {
        /// <summary>
        /// Data for test <see cref="ParsePekaoBPFile"/>
        /// <para>First object:  string - name of file from Samples directory</para>
        /// <para>Second object: ReportStatement[] - expected result from parsing</para>
        /// </summary>
        public static IEnumerable<object[]> PekaoBPData
            => new List<object[]>
            {
                new object[]
                {
                    @"PekaoBPSample.txt",
                    //"windows-1250",
                    "utf-8",
                    new ReportStatement[]
                    {
                        new ReportStatement
                        {
                            AccountIdentifier = "PL44102055610000380209739045",
                            SequenceNumber = "1",
                            OpeningBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Debit,
                                Date = new DateTime(2021, 5, 26),
                                Currency = "PLN",
                                Amount = 2671.79m
                            },
                            Transactions = new List<ReportStatementData.Transaction>
                            {
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2021, 5, 26),
                                    EntryDate = new DateTime(1, 5, 26),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 25m,
                                    Type = "152",
                                    CustomerReference = "NONREF",
                                    BankReference = "6460500500000513",
                                    SupplementaryDetails = "152 0",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "020~00152",
                                            "~20PRZELEW SRODKÓW",
                                            "~21˙",
                                            "~22˙",
                                            "~23˙",
                                            "~24˙",
                                            "~25˙",
                                            "~3010205561",
                                            "~319000361245650140",
                                            "~32FSDFSFDSF",
                                            "~33˙",
                                            "~38PL50102055619000361245650240",
                                            "~60˙",
                                            "~63˙"
                                        },
                                        ReceiverName = "FSDFSFDSF",
                                        ReceiverIBAN = "PL50102055619000361245650240",
                                        TransferTitle = "PRZELEW SRODKÓW"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2021, 5, 26),
                                    EntryDate = new DateTime(1, 5, 26),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 434m,
                                    Type = "210",
                                    CustomerReference = "NONREF",
                                    BankReference = "6460502100001611",
                                    SupplementaryDetails = "210 0",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "020~00210",
                                            "~20P 85100158550    0       PI",
                                            "~21T-23",
                                            "~22˙",
                                            "~23˙",
                                            "~24˙",
                                            "~25˙",
                                            "~3010100071",
                                            "~312223147244000000",
                                            "~32DRUGI MAZOWIECKI URZˇD SKAR",
                                            "~33BOWY WARSZAWA",
                                            "~38PL32101000712223147254000000",
                                            "~60˙",
                                            "~63˙"
                                        },
                                        ReceiverName = "DRUGI MAZOWIECKI URZˇD SKARBOWY WARSZAWA",
                                        ReceiverIBAN = "PL32101000712223147254000000",
                                        TransferTitle = "P 85100158550    0       PIT-23"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2021, 5, 26),
                                    EntryDate = new DateTime(1, 5, 26),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 205.18m,
                                    Type = "107",
                                    CustomerReference = "NONREF",
                                    BankReference = "6463600500000059",
                                    SupplementaryDetails = "107 0",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "020~00107",
                                            "~20PRZELEW SRODKÓW",
                                            "~21˙",
                                            "~22˙",
                                            "~23˙",
                                            "~24˙",
                                            "~25˙",
                                            "~30˙",
                                            "~31˙",
                                            "~32IRENA KOWALSKA",
                                            "~33˙",
                                            "~38FR7630004013280001089882824",
                                            "~60˙",
                                            "~63˙"
                                        },
                                        ReceiverName = "IRENA KOWALSKA",
                                        ReceiverIBAN = "FR7630004013280001089882824",
                                        TransferTitle = "PRZELEW SRODKÓW"
                                    }
                                },
                                new ReportStatementData.Transaction
                                {
                                    ValueDate = new DateTime(2021, 5, 26),
                                    EntryDate = new DateTime(1, 5, 26),
                                    ValueSign = ReportStatementData.DebitCredit.Debit,
                                    Value = 0.75m,
                                    Type = "108",
                                    CustomerReference = "NONREF",
                                    BankReference = "6463600500000060",
                                    SupplementaryDetails = "108 0",
                                    Details = new ReportStatementData.TransactionDetails
                                    {
                                        RawData = new List<string>
                                        {
                                            "020~00108",
                                            "~20KOSZTY SR21IP00012613DS",
                                            "~21˙",
                                            "~22˙",
                                            "~23˙",
                                            "~24˙",
                                            "~25˙",
                                            "~30˙",
                                            "~31˙",
                                            "~32IRENA KOWALSKA",
                                            "~33˙",
                                            "~38FR7630004013280001089882824",
                                            "~60˙",
                                            "~63˙"
                                        },
                                        ReceiverName = "IRENA KOWALSKA",
                                        ReceiverIBAN = "FR7630004013280001089882824",
                                        TransferTitle = "KOSZTY SR21IP00012613DS"
                                    }
                                }
                            },
                            ClosingBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Debit,
                                Date = new DateTime(2021, 5, 31),
                                Currency = "PLN",
                                Amount = 3336.72m
                            },
                            ClosingAvailableBalance = new ReportStatementData.Balance
                            {
                                BalanceSign = ReportStatementData.DebitCredit.Credit,
                                Date = new DateTime(2021, 5, 31),
                                Currency = "PLN",
                                Amount = 91662.28m
                            }
                        }
                    }
                }
            };

        [Theory]
        [MemberData(nameof(PekaoBPData))]
        public void ParsePekaoBPFile(string fileName, string encodingName, ReportStatement[] expectedStatements)
        {
            PekaoBPParser parser = new(',');

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(encodingName);
                        
            string fileContent = Sample.Get(fileName, encoding);

            ReportStatement[] statements = parser.Parse(fileContent).ToArray();

            Assert.Equal(expectedStatements, statements);
        }
    }
}
