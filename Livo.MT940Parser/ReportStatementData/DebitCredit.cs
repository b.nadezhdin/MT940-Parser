﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using System;

namespace Livo.MT940Parser.ReportStatementData
{
    public enum DebitCredit
    {
        /// <summary>
        /// Debit, negative balance.
        /// </summary>
        Debit,
        /// <summary>
        /// Used in some banks, no idea
        /// </summary>
        ReversalOfDebit,
        /// <summary>
        /// Credit, positive balance (or 0).
        /// </summary>
        Credit,
        /// <summary>
        /// Used in some banks, no idea
        /// </summary>
        ReversalOfCredit
    }

    /// <summary>
    /// Factory creating enum based on the string value.
    /// </summary>
    public static class DebitCreditFactory
    {
        /// <summary>
        /// Creayes DebitCredit enum based on the string value.
        /// </summary>
        public static DebitCredit GetDebitCredit(string value)
        {
            switch(value)
            {
                case "D":
                    return DebitCredit.Debit;
                case "RD":
                    return DebitCredit.ReversalOfDebit;
                case "C":
                    return DebitCredit.Credit;
                case "RC":
                    return DebitCredit.ReversalOfCredit;
                default:
                    throw new ArgumentException("Invalid value for DebitCredit: " + value);
            }
        }
    }
}
