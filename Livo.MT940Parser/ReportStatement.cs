﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;

namespace Livo.MT940Parser
{
    /// <summary>
    /// MT940-Statement
    /// </summary>
    public class ReportStatement
    {
        /// <summary>
        /// Unambiguous identifier of the transaction.
        /// </summary>
        public string TransactionReferenceNumber { get; set; }

        /// <summary>
        /// Reference number of the message when MT940 is sent as a response for MT920.
        /// </summary>
        public string RelatedReference { get; set; }

        /// <summary>
        /// Account identifier, usually IBAN preceeded.
        /// </summary>
        public string AccountIdentifier { get; set; }

        /// <summary>
        /// Statement/Sequence number.
        /// </summary>
        public string SequenceNumber { get; set; }

        /// <summary>
        /// (Intermediate) opening balance.
        /// </summary>
        public Balance OpeningBalance { get; set; }

        /// <summary>
        /// (Intermediate) closing balance.
        /// </summary>
        public Balance ClosingBalance { get; set; }

        /// <summary>
        /// Funds which are available (or subject to intrest charges) to the account owner (if credit balance) or the balance which is subject to interest charges (if debit balance).
        /// </summary>
        public Balance ClosingAvailableBalance { get; set; }

        /// <summary>
        /// Funds which are available (for specified forward value date) to the account owner (if credit balance) or the balance which is subject to interest charges (if debit balance).
        /// </summary>
        public Balance ForwardAvailableBalance { get; set; }

        /// <summary>
        /// Transactions
        /// </summary>
        public List<Transaction> Transactions { get; set; }

        public override bool Equals(object obj)
        {
            ReportStatement statement = obj as ReportStatement;
            if (statement == null )
            {
                return false;
            }

            // Both lists must have the same length or both be null
            if ( ( Transactions?.Count ?? -1 ) != ( statement.Transactions?.Count ?? -1 ) )
            {
                return false;
            }

            bool fieldsEqual =
                   TransactionReferenceNumber == statement.TransactionReferenceNumber &&
                   RelatedReference == statement.RelatedReference &&
                   AccountIdentifier == statement.AccountIdentifier &&
                   SequenceNumber == statement.SequenceNumber &&
                   EqualityComparer<Balance>.Default.Equals(OpeningBalance, statement.OpeningBalance) &&
                   EqualityComparer<Balance>.Default.Equals(ClosingBalance, statement.ClosingBalance) &&
                   EqualityComparer<Balance>.Default.Equals(ClosingAvailableBalance, statement.ClosingAvailableBalance) &&
                   EqualityComparer<Balance>.Default.Equals(ForwardAvailableBalance, statement.ForwardAvailableBalance);

            if ( Transactions == null || statement.Transactions == null )
                return fieldsEqual;

            bool transactionsEqual = true;

            for ( int i = 0; i < Transactions.Count; i++ )
            {
                transactionsEqual &= Transactions[ i ].Equals(statement.Transactions[ i ]);
            }

            return fieldsEqual & transactionsEqual;
        }

        public override int GetHashCode()
        {
            int hashCode = -234613761;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TransactionReferenceNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(RelatedReference);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AccountIdentifier);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SequenceNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<Balance>.Default.GetHashCode(OpeningBalance);
            hashCode = hashCode * -1521134295 + EqualityComparer<Balance>.Default.GetHashCode(ClosingBalance);
            hashCode = hashCode * -1521134295 + EqualityComparer<Balance>.Default.GetHashCode(ClosingAvailableBalance);
            hashCode = hashCode * -1521134295 + EqualityComparer<Balance>.Default.GetHashCode(ForwardAvailableBalance);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Transaction>>.Default.GetHashCode(Transactions);
            return hashCode;
        }
    }
}
