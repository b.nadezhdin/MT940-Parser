﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportParser.TagParser;

namespace Livo.MT940Parser.ReportParser
{
    /// <summary>
    /// Parser with attached SWIFT tag parsers - <see cref="SWIFTTagParser"/>
    /// </summary>
    public class SWIFTReportParser : ReportParserBase
    {
        protected readonly char _decimalSeparator;

        public SWIFTReportParser(string statementHeader, string statementTrailer, char decimalSeparator) : base(statementHeader, statementTrailer)
        {
            _decimalSeparator = decimalSeparator;
        }

        protected override void ParseTag20(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag20(command, reportStatement);

        protected override void ParseTag21(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag21(command, reportStatement);

        protected override void ParseTag25(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag25(command, reportStatement);

        protected override void ParseTag28(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag28(command, reportStatement);

        protected override void ParseTag28C(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag28(command, reportStatement);

        protected override void ParseTag60a(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag60(command, reportStatement, _decimalSeparator);

        protected override void ParseTag60m(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag60(command, reportStatement, _decimalSeparator);

        protected override void ParseTag60F(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag60(command, reportStatement, _decimalSeparator);

        protected override void ParseTag60M(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag60(command, reportStatement, _decimalSeparator);

        protected override void ParseTag61(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag61(command, reportStatement, _decimalSeparator);

        protected override void ParseTag62a(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag62(command, reportStatement, _decimalSeparator);

        protected override void ParseTag62m(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag62(command, reportStatement, _decimalSeparator);

        protected override void ParseTag62F(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag62(command, reportStatement, _decimalSeparator);

        protected override void ParseTag62M(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag62(command, reportStatement, _decimalSeparator);

        protected override void ParseTag64(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag64(command, reportStatement, _decimalSeparator);

        protected override void ParseTag65(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag65(command, reportStatement, _decimalSeparator);

        protected override void ParseTag86(ReportCommand command, ReportStatement reportStatement)
            => SWIFTTagParser.Tag86(command, reportStatement);
    }
}
