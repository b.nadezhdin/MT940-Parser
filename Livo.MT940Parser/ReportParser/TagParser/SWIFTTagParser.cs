﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System.Collections.Generic;
using System.Linq;

namespace Livo.MT940Parser.ReportParser.TagParser
{
    /// <summary>
    /// Parsers for tags according to SWIFT MT940 format
    /// <para><a href="https://www.sepaforcorporates.com/swift-for-corporates/account-statement-mt940-file-format-overview/" /></para>
    /// </summary>
    public static class SWIFTTagParser
    {
        /// <summary>
        /// Tag 20 - holds message identifier.
        /// </summary>
        public static void Tag20(ReportCommand command, ReportStatement reportStatement)
        {
            reportStatement.TransactionReferenceNumber = command.Data[ 0 ];
        }

        /// <summary>
        /// Tag 21 - holds reference to message if MT940 is a response for MT920.
        /// </summary>
        public static void Tag21(ReportCommand command, ReportStatement reportStatement)
        {
            reportStatement.RelatedReference = command.Data[ 0 ];
        }

        /// <summary>
        /// Tag 25 - holds account identifier.
        /// </summary>
        public static void Tag25(ReportCommand command, ReportStatement reportStatement)
        {
            reportStatement.AccountIdentifier = command.Data[ 0 ];
        }

        /// <summary>
        /// Tag 28 - holds statement number + eventually sequence number.
        /// </summary>
        public static void Tag28(ReportCommand command, ReportStatement reportStatement)
        {
            reportStatement.SequenceNumber = command.Data[ 0 ];
        }

        /// <summary>
        /// Tag 60 - opening balance, can be present with variants F and M, depending from when the balance is.
        /// </summary>
        public static void Tag60(ReportCommand command, ReportStatement reportStatement, char decimalSeparator)
        {
            reportStatement.OpeningBalance = UtilityParser.BalanceParser.SWIFTBalance(command.Data[ 0 ], decimalSeparator);
        }

        /// <summary>
        /// Tag 61 - adds new transaction.
        /// </summary>
        public static void Tag61(ReportCommand command, ReportStatement reportStatement, char decimalSeparator)
        {
            if ( reportStatement.Transactions == null )
            {
                reportStatement.Transactions = new List<Transaction>();
            }

            Transaction transaction = UtilityParser.TransactionParser.SWIFTTransaction(command.Data[ 0 ], decimalSeparator);
            if ( command.Data.Count > 1 )
            {
                transaction.SupplementaryDetails = command.Data[ 1 ];
                for ( int i = 2; i < command.Data.Count; i++ )
                {
                    transaction.SupplementaryDetails += "\r\n" + command.Data[ i ];
                }
            }
            reportStatement.Transactions.Add(transaction);
        }

        /// <summary>
        /// Tag 62 - closing balance, can be present with variants F and M, depending from when the balance is.
        /// </summary>
        public static void Tag62(ReportCommand command, ReportStatement reportStatement, char decimalSeparator)
        {
            reportStatement.ClosingBalance = UtilityParser.BalanceParser.SWIFTBalance(command.Data[ 0 ], decimalSeparator);
        }

        /// <summary>
        /// Tag 64 - closing available balance.
        /// </summary>
        public static void Tag64(ReportCommand command, ReportStatement reportStatement, char decimalSeparator)
        {
            reportStatement.ClosingAvailableBalance = UtilityParser.BalanceParser.SWIFTBalance(command.Data[ 0 ], decimalSeparator);
        }

        /// <summary>
        /// Tag 65 - forward available balance.
        /// </summary>
        public static void Tag65(ReportCommand command, ReportStatement reportStatement, char decimalSeparator)
        {
            reportStatement.ForwardAvailableBalance = UtilityParser.BalanceParser.SWIFTBalance(command.Data[ 0 ], decimalSeparator);
        }

        /// <summary>
        /// Tag 86 - saves raw data about the details, since there is no specified format.
        /// </summary>
        public static void Tag86(ReportCommand command, ReportStatement reportStatement)
        {
            if ( reportStatement.Transactions == null || reportStatement.Transactions.Count == 0 )
            {
                return;
            }

            Transaction lastTransaction = reportStatement.Transactions.Last();

            if ( lastTransaction.Details == null )
            {
                lastTransaction.Details = new TransactionDetails();
            }

            if ( lastTransaction.Details.RawData == null )
            {
                lastTransaction.Details.RawData = new List<string>();
            }

            lastTransaction.Details.RawData.AddRange(command.Data);
        }
    }
}
