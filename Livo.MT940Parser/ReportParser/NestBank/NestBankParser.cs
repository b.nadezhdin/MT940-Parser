﻿// Copyright (c) LivoLink. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.

using Livo.MT940Parser.ReportStatementData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Livo.MT940Parser.ReportParser.NestBank
{
    /// <summary>
    /// Parser for the MT940 format of the Nest Bank
    /// <para>No documentation about the format was found</para>
    /// </summary>
    public class NestBankParser : SWIFTReportParser
    {
        public NestBankParser(char decimalSepatator) : base("", "", decimalSepatator)
        {
        }

        protected override string PreprocessFileContent(string mt940FileContent)
        {
            mt940FileContent = base.PreprocessFileContent(mt940FileContent);

            string[] splitted = mt940FileContent.Split("<".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < splitted.Length; i++)
            {
                if (!splitted[ i ].EndsWith("\r\n"))
                {
                    splitted[ i ] += "\r\n";
                }
            }

            string joined = string.Join("<", splitted);

            return joined;
        }

        protected override bool ContainsHeadersAndTrailers(string mt940FileContent)
        {
            // There is no header or trailer at all in this bank
            return false;
        }

        protected override void ParseTag20(ReportCommand command, ReportStatement reportStatement)
        { 
            // Do nothing - in this bank, this is just a constant value "MT940"
            return;
        }

        protected override void ParseTag61(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag61(command, reportStatement);

            Transaction transaction = reportStatement.Transactions.Last();
            if(transaction.CustomerReference.EndsWith("//"))
            {
                transaction.CustomerReference = transaction.CustomerReference.Substring(0, transaction.CustomerReference.Length - 2);
            }
        }

        protected override void ParseTag86(ReportCommand command, ReportStatement reportStatement)
        {
            base.ParseTag86(command, reportStatement);

            StringBuilder titleBuilder = new StringBuilder();
            StringBuilder receiverNameBuilder = new StringBuilder();
            StringBuilder ibanBuilder = new StringBuilder();

            StringBuilder lastBuilder = null;

            foreach (string line in command.Data)
            {
                if(!string.IsNullOrEmpty(line) && !line.StartsWith("<") && lastBuilder != null && line != command.Data.LastOrDefault())
                {
                    lastBuilder.Append(line);
                    continue;
                }

                string lineValue = line.Substring(3);

                // Line is empty - nothing to read
                if (string.IsNullOrEmpty(lineValue))
                {
                    continue;
                }

                if (line.StartsWith("<2") && "0123456".Contains(line[ 2 ])) // From <20 to <26
                {
                    titleBuilder.Append(lineValue);
                    lastBuilder = titleBuilder;
                }

                if ( (line.StartsWith("<2") && "789".Contains(line[ 2 ]))
                    || line.StartsWith("<60") ) // From <27 to <29 and <60
                {
                    receiverNameBuilder.Append(lineValue + " ");
                    lastBuilder = receiverNameBuilder;
                }

                if (line.StartsWith("<38"))
                {
                    ibanBuilder.Append(lineValue);
                    lastBuilder = ibanBuilder;
                }
            }

            Transaction transaction = reportStatement.Transactions.Last();
            TransactionDetails transactionDetails = transaction.Details;

            if (titleBuilder.Length > 0)
                transactionDetails.TransferTitle = titleBuilder.ToString();
            if (receiverNameBuilder.Length > 0)
                transactionDetails.ReceiverName = receiverNameBuilder.ToString().Trim();
            if (ibanBuilder.Length > 0)
                transactionDetails.ReceiverIBAN = ibanBuilder.ToString();

            // Remove quotes from the title for readablity - they are present in all samples that were gathered
            if (transactionDetails.TransferTitle.Length > 1 
                    && transactionDetails.TransferTitle[0] == '"' 
                    && transactionDetails.TransferTitle[ transactionDetails.TransferTitle.Length - 1] == '"')
            {
                transactionDetails.TransferTitle = transactionDetails.TransferTitle.Substring(1, transactionDetails.TransferTitle.Length - 2);
            }

        }
    }
}
